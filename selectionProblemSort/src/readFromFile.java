import java.io.File;
import java.io.FileNotFoundException;
import java.text.CollationElementIterator;
import java.util.*;

/**
 * Name: Chelsea Smith
 * Class: CSCI 3200
 * Instructor: Justin Turner
 * Date: 06/02/2015
 * 1.1: Write a program to solve the selection problem (find a given �k� in a group of N numbers),
 * k = N/2. Draw a table showing running time of your program for various values of N. (10, 100, 1000, 1 million)
 * 1. One way to solve this problem would be to read the N numbers into an array, sort the
 * array in decreasing order by some simple algorithm such as bubblesort, and then return
 * the element in position k.
 * 2. A somewhat better algorithm might be to read the first k elements into an array and
 * sort them (in decreasing order). Next, each remaining element is read one by one. As a new
 * element arrives, it is ignored if it is smaller than the kth element in the array. Otherwise, it
 * is placed in its correct spot in the array, bumping one element out of the array. When the
 * algorithm ends, the element in the kth position is returned as the answer.
 */

public class readFromFile
{

    public static void main(String[] args) throws FileNotFoundException
    {
        long startTime = System.nanoTime();
        long endTime = 0;
        Random r = new Random();
        File file = new File(".\\src\\random1000000.txt");
        Scanner scan = new Scanner(file); //open file
        ArrayList<Integer> random1000000 = new ArrayList<>();
        while (scan.hasNext())
        {
            try
            {
                random1000000.add(scan.nextInt());

            }catch (Exception e) {
                scan.next();
                System.out.println(e.getMessage());
            }
        }
        scan.close();
        ArrayList<Integer>nums = random1000000;
        Collections.sort(nums);
        Collections.reverse(nums);
        int k = nums.get(nums.size()/2);
        System.out.println("1,000,000: K = "+k);
        if (random1000000.get(k-1) == k){
             endTime = System.nanoTime();
            System.out.println("Runtime 1,000,000 Numbers:" + String.format( "%12.6f",((endTime-startTime)/Math.pow(10,9))));
        }
        else{
            boolean isDone = false;
            for (int i = 0; i < nums.size();i++){
                if (isDone){
                    break;
                }
                if (k==random1000000.get(i)){
                    endTime = System.nanoTime();
                    System.out.println("Runtime 1,000,000 Numbers:" + String.format( "%12.6f",((endTime-startTime)/Math.pow(10,9))));
                    isDone = true;
                }
            }
        }
        startTime = System.nanoTime();
        ArrayList<Integer>list100 = new ArrayList<>();
        for (int i = 0; i < 100; i++){
            list100.add(random1000000.get(r.nextInt(1000000)));
        }
        Collections.sort(list100);
        Collections.reverse(list100);
        System.out.println("100 Array List: " +list100);
        k = list100.get(list100.size()/2);
        System.out.println("100: K = "+k);
            boolean isDone = false;
            for (int i = 0; i < list100.size();i++){
                if (isDone){
                    break;
                }
                if (k==list100.get(i)){
                    endTime = System.nanoTime();
                    System.out.println("Runtime 100 Numbers:" + String.format( "%12.6f",((endTime-startTime)/Math.pow(10,9))));
                    isDone = true;
                }
            }
        startTime = System.nanoTime();
        ArrayList<Integer>list10 = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            list10.add(random1000000.get(r.nextInt(1000000)));
        }
        Collections.sort(list10);
        Collections.reverse(list10);
        k = list10.get(list10.size()/2);
        System.out.println("10: K = "+k);
        System.out.println("10 Array List: " +list10);
             isDone = false;
            for (int i = 0; i < list10.size();i++) {
                if (isDone) {
                    break;
                }
                if (k == list10.get(i)) {
                    endTime = System.nanoTime();
                    System.out.println("Runtime 10 Numbers:" + String.format("%12.6f", ((endTime - startTime) / Math.pow(10, 9))));
                    isDone = true;
                }
            }
    }
}

