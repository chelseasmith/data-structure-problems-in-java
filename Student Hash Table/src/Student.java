/**
 * Created by cjsmith1102 on 7/6/2015.
 */

public class Student {
    private String name;
    private int age;
    private int gradYear;
    public Student(String n, int a, int gy)
    {
        name = n;
        age = a;
        gradYear = gy;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        //result = prime * result + age;
        result = prime * result + gradYear;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public String toString() {
        return "Student [name=" + name + ", age=" + age + ", gradYear="
                + gradYear + ", " + hashCode() + "]";
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (age != other.age)
            return false;
        if (gradYear != other.gradYear)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}

