/**
 * Created by cjsmith1102 on 7/6/2015.
 */

public class SeparateChainingTester {

    public static void main(String[] args) {
        Student s1 = new Student("JET", 30, 2007);
        Student s2 = new Student("Bill", 18, 2012);
        Student s3 = new Student("Bob", 21, 2015);

        SeparateChaining sc = new SeparateChaining(1);
        sc.insert(s1);
        sc.insert(s2);
        sc.insert(s3);

        System.out.println(sc);

    }

}
