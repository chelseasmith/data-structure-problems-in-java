import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cjsmith1102 on 7/6/2015.
 */


public class SeparateChaining<A> {
    private static final int DEFAULTSIZE = 101;
    private List<A>[] hashTable;
    private int currentCount;
    public SeparateChaining()
    {
        this(DEFAULTSIZE);
    }
    public SeparateChaining(int size)
    {
        hashTable = new LinkedList[nextPrime(size)];
        makeEmpty();
    }
    private int nextPrime(int size)
    {
        BigInteger b = BigInteger.valueOf(size);
        return b.nextProbablePrime().intValue();
    }
    public void makeEmpty()
    {
        for(int i = 0; i < hashTable.length; i++)
            hashTable[i] = new LinkedList<A>();
        currentCount = 0;
    }
    @Override
    public String toString() {
        return "SeparateChaining [hashTable=" + Arrays.toString(hashTable)
                + ", currentCount=" + currentCount + "]";
    }
    private int myHash(A item)
    {
        int hashVal = item.hashCode();

        hashVal = hashVal % hashTable.length;
        if(hashVal < 0)
            hashVal += hashTable.length;

        return hashVal;
    }
    public void insert(A item)
    {
        List<A> hashRow = hashTable[myHash(item)];
        if(!hashRow.contains(item))
        {
            hashRow.add(item);
            currentCount++;
        }
    }
    public void remove(A item)
    {
        List<A> hashRow = hashTable[myHash(item)];
        if(hashRow.contains(item))
        {
            hashRow.remove(item);
            currentCount--;
        }
    }
    public boolean contains(A item)
    {
        List<A> hashRow = hashTable[myHash(item)];
        if(hashRow.contains(item))
        {
            return true;
        }
        return false;
    }
}
