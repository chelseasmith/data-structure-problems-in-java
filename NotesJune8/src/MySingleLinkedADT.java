/**
 * Created by cjsmith1102 on 6/8/2015.
 */
public class MySingleLinkedADT
{
    Node first;

    public MySingleLinkedADT()
    {
        first = null;
    }
    public void add(int val)
    {
        Node newData = new Node(val);
        Node current;
        if(first == null)
        {
            first =newData;
        }
        else
        {
            current = first;
            while(current.next !=null)
            {
                current = current.next;
            }
            current.next = newData;
        }
    }

    private class Node{
        Node next;
        int val;
        public Node(int val){
            next = null;
        }
    }
}
