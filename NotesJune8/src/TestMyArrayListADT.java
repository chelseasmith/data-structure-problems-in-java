/**
 * Created by cjsmith1102 on 6/8/2015.
 */

public class TestMyArrayListADT {

    public static void main(String[] args) {
        MyArrayListADT adt = new MyArrayListADT();
        for(int i = 0; i < 10; i++)
            adt.add(i);
        adt.printList();
        adt.insert(10, 5);
        adt.printList();
        adt.remove(5);
        adt.printList();
        adt.insert(5, 5);
        adt.insert(5, 1);
        adt.insert(5, 10);
        adt.printList();
        adt.remove(5);
        adt.printList();
        adt.printSizes();
    }

}

