/**Assignments are constant time.
 * if everything else is constant time then the loop as a whole is constant time
 * function called within the program is also constant time (for our purposes)
 * while loop with a for loop in it is 0(N)
 * then overall the method is 0(N)
 * you just add the times together
 * toString method is the opposite: how long does it take to run?
 * * look at all of the different pieces
 * * simple toString first and then the more complex one
 * * getChars is similar to for loop so it is o(N)
 * Function overall is 0(N) for toString
 * while loops are 0(N)
 *
 * Chapter 3:
 *
 * ADT: set of objects within a set of operations
 * may have methods
 * lists, stacks, and queues are examples of ADT
 *
 * List ADT: coma
 * zero is an empty list
 * examples: printList, makeEmpty, etc
 *
 * Array Implementation: limitation of java is that the array size is static so you can't change it
 *
 * int[] arr = new int[];
 *then create a new array and put the information in it
 * printList0(N)
 * findKth(index) 0(1)
 * insert and remove can be either 0(1) or 0(N)
 *
 */

public class Notes {
}
