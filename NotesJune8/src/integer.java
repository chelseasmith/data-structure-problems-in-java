/**
 * Created by cjsmith1102 on 6/8/2015.
 */
public class integer{

    public static int parseInt(String s) throws NumberFormatException
    {
        return parseInt(s,10);
    }
    public static int parseInt(String s, int radix) throws NumberFormatException
    {
        if (s == null)
        {
            throw new NumberFormatException("null");
        }
        if (radix < Character.MIN_RADIX)
        {
            throw new NumberFormatException("radix " + radix + " less than Character.MIN_RADIX");
        }
        if (radix > Character.MAX_RADIX)
        {
            throw new NumberFormatException("radix " + radix + " greater than Character.MAX_RADIX");
        }
        int result = 0;
        boolean negative = false;
        int i = 0, len = s.length();
        int limit = -Integer.MAX_VALUE;
        int multmin;
        int digit;
        if (len > 0)
        {
            char firstChar = s.charAt(0);
            if (firstChar < '0')
            { // Possible leading "-"
                if (firstChar == '-')
                {
                    negative = true;
                    limit = Integer.MIN_VALUE;
                }
                else
                    throw NumberFormatException.forInputString(s);
                if (len == 1) // Cannot have lone "-"
                    throw NumberFormatException.forInputString(s);
                i++;
            }
            multmin = limit / radix;
            while (i < len)
            {
                // Accumulating negatively avoids surprises near MAX_VALUE
                digit = Character.digit(s.charAt(i++),radix);
                if (digit < 0)
                {
                    throw NumberFormatException.forInputString(s);
                }
                if (result < multmin)
                {
                    throw NumberFormatException.forInputString(s);
                }
                result *= radix;
                if (result < limit + digit)
                {
                    throw NumberFormatException.forInputString(s);
                }
                result -= digit;
            }
        }
        else
        {
            throw NumberFormatException.forInputString(s);
        }
        return negative ? result : -result;
    }



    public static String toString(int i, int radix)
    {
        if (radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
            radix = 10;
	/* Use the faster version */
        if (radix == 10)
        {
            return toString(i);
        }
        char buf[] = new char[33];
        boolean negative = (i < 0);
        int charPos = 32;
        if (!negative)
        {
            i = -i;
        }
        while (i <= -radix)
        {
            buf[charPos--] = digits[-(i % radix)];
            i = i / radix;
        }
        buf[charPos] = digits[-i];
        if (negative)
        {
            buf[--charPos] = '-';
        }
        return new String(buf, charPos, (33 - charPos));
    }
    public static String toString(int i)
    {
        if (i == Integer.MIN_VALUE)
            return "-2147483648";
        int size = (i < 0) ? stringSize(-i) + 1 : stringSize(i);
        char[] buf = new char[size];
        getChars(i, size, buf);
        return new String(0, size, buf);
    }
    static int stringSize(int x)
    {
        for (int i=0; ; i++)
            if (x <= sizeTable[i])
                return i+1;
    }
    final static int [] sizeTable = { 9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999, Integer.MAX_VALUE };
}
