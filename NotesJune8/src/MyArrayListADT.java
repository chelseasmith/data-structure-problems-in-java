/**
 * Created by cjsmith1102 on 6/8/2015.
 */

public class MyArrayListADT {
    private int[] list;
    private int size;

    public MyArrayListADT()
    {
        list = new int[10];
        size = 0;
    }
    public void add(int val)
    {
        ensureCapacity(size+1);//ensure space in the array
        list[size] = val;
        size++;
    }
    public void printList()
    {
        for(int i = 0; i < size; i++)
        {
            System.out.print(list[i] + ", ");
        }
        System.out.println();
    }
    public void insert(int val, int index)
    {
        if(index < size)
        {
            if(index < 0)//ensure index is 0 or greater
                index = 0;
            ensureCapacity(size+1);//ensure space in the array
            //loop runs from size to index+1 to shift everything to the right
            for(int i = size; i > index; i--)
            {
                //shift each item one position to the right
                list[i] = list[i-1];
            }
            //makes room for the new value at the requested index
            list[index] = val;
            size++;
        }
        else//index past end of list, just add val at end of list
        {
            add(val);
        }
    }
    public int delete(int index)
    {
        if(index < size && index > 0)//ensure index is within bounds of list
        {
            //if index is last item, just reduce the size and return the item
            if(index == size-1)
            {
                size--;
                return list[index];
            }
            //store current value before overwritting
            int temp = list[index];
            //shift items to the left
            for(int i = index; i < size; i++)
            {
                list[i] = list[i+1];
            }
            size--;
            return temp;
        }
        else
            return -1;
    }
    public int find(int val)
    {
        for(int i = 0; i < size; i++)
        {
            if(list[i] == val)
            {
                return i;
            }
        }
        return -1;
    }
    public void remove(int val)
    {
        int index = find(val);//find index of value
        delete(index);//delete value by index
    }
    public int get(int index)
    {
        if(index < size)
            return list[index];
        else
            return -1;
    }
    public int next(int index)
    {
        if(index + 1 < size)
            return list[index+1];
        else
            return -1;
    }
    public int previous(int index)
    {
        if(index - 1 < size)
            return list[index-1];
        else
            return -1;
    }
    public void ensureCapacity(int newSize)
    {
        if(newSize > list.length)
            growArray();
    }
    private void growArray()
    {
        int[] newArr = new int[list.length*2];
        for(int i = 0; i < size; i++)
        {
            newArr[i] = list[i];
        }
        list = newArr;
    }
    public void printSizes()
    {
        System.out.println("Array Size:" + list.length);
        System.out.println("List Size:" + size);
    }
}
