/**
 * Created by cjsmith1102 on 6/15/2015.
 */
/**
 * Created by cjsmith1102 on 6/14/2015.
 */
public class MyLinkedListADT {
    Node first;

    public MyLinkedListADT()
    {
        first = null;
    }
    public void add(int val)
    {
        Node newData = new Node(val);
        Node current;
        if(first == null)
        {
            first = newData;
        }
        else
        {
            current = first;
            while(current.next != null)
            {
                current = current.next;
            }
            current.next = newData;
        }
    }
    public void printList()
    {
        Node current;
        if(first != null)
        {
            current = first;
            while(current != null){
                System.out.print(current.val + ", ");
                current = current.next;

            }
            System.out.println();
        }
        else{
            System.out.println("list is null");
        }
    }
    public void insert(int val, int index)
    {
        Node newData = new Node(val);
        Node current, temp;
        int currentIndex;
        if(first == null)
        {
            first = newData;
        }
        else
        {
            current = first;
            currentIndex = 0;
            while(current.next != null && currentIndex < index)
            {
                current = current.next;
                currentIndex++;
            }
            //node 1, node 2
            temp = current.next;//pointer to node 2
            current.next = newData;//node 1 now points to newData
            newData.next = temp;//newData now points to node 2
            //node 1, newData, node 2
        }
    }
    public void delete(int val) {
        if(first == null){
            return;
        }

        Node prevNode = null;
        Node currNode = first;
        while (currNode != null && currNode.val != val) {
            prevNode = currNode;
            currNode = currNode.next;

            if (prevNode == null) {
                first.val = -1;
                return;
            }
            if (currNode == null) {
                System.out.println("Empty Node");
                return;
            }
        }
        currNode.val = -1;
    }

    public int find(int val)
    {
        Node current;
        int position;
        if(first == null)
        {
            return -1;
        }
        else
        {
            current = first;
            position = 0;
            while(current.next != null)
            {
                if(current.val != val)
                {
                    current = current.next;
                    position++;
                }
                else
                    return position;
            }
            return -1;
        }
    }
    public void swap (int val1, int val2)
    {
        Node current;
        int position;
        int pos1 = this.find(val1);
        int pos2 = this.find(val2);
        if(first == null)
        {
            return;
        }
        else
        {

            this.insert(val1,pos2);
            this.delete(pos2-1);
            this.insert(val2,pos1);
            this.delete(pos1-1);
        }
    }
    // LOGIC FOR PROGRAM

    /**
     * 1. when delete DO NOT delete it just place a "G" in place of the value at the position.
     *
     */
    private class Node{
        public Node next;
        public int val;
        public Node(int v)
        {
            val = v;
            next = null;
        }
    }
    public static void main (String []args){
        MyLinkedListADT adt = new MyLinkedListADT();
        for (int i = 0 ; i < 20; i++){
            adt.add(i+1);
        }
        adt.printList();
        adt.delete(7);
        adt.printList();
    }
}