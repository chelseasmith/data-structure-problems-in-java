package com.company;

import java.util.ArrayList;
import java.util.Arrays;


public class Sorting {

    public static void main(String[] args) {

        String[] strarr = new String[]{"Word","word","WORD","wOrD"};
        System.out.println(Arrays.toString(strarr));
        radixSortStrings(strarr, 4);
        System.out.println(Arrays.toString(strarr));
		/*
		Integer[] arr = new Integer[]{81, 94, 11, 96, 12, 35, 17, 95, 28, 58, 41, 75, 15};
		System.out.println(Arrays.toString(arr));
		mergeSort(arr);
		System.out.println(Arrays.toString(arr));
		*/
    }
    public static void mergeSort(Integer[] arr)
    {
        //call mergeSort(arr, temp[], 0, length-1)
        mergeSort(arr, new Integer[arr.length], 0, arr.length - 1);
    }
    private static void mergeSort(Integer[] arr, Integer[] temp, int left, int right)
    {
        //if left < right
        if(left < right)
        {
            //find center
            int center = (right + left) / 2;

            System.out.println("Split List: " + left + " : " + center + " : " + right + " :\n\tLEFT: "
                    + Arrays.toString(Arrays.copyOfRange(arr, left, center+1)) + " \n\tRIGHT: "
                    + Arrays.toString(Arrays.copyOfRange(arr, center+1, right+1)));

            //call mergeSort on left half
            mergeSort(arr, temp, left, center);
            //call mergeSort on right half
            mergeSort(arr, temp, center + 1, right);
            //call merge over left/right halves
            merge(arr, temp, left, center + 1, right);
        }
    }
    private static void merge(Integer[] arr, Integer[] temp,
                              int leftStart, int rightStart, int rightEnd)
    {
        //determine leftEnd
        int leftEnd = rightStart - 1;
        //set temp array position (same as left start)
        int tempPos = leftStart;
        //determine number of elements (end - start + 1)
        int numberElements = rightEnd - leftStart + 1;

        int startPrint = leftStart;
        int endPrint = rightEnd;
        System.out.println("Begin Merge: " + numberElements + " :" +
                "\n\tLEFT: " + Arrays.toString(Arrays.copyOfRange(arr, leftStart, leftEnd+1)) +
                "\n\tRIGHT: " + Arrays.toString(Arrays.copyOfRange(arr, rightStart, rightEnd+1)));

        //while items left in both lists
        while(leftStart <= leftEnd && rightStart <= rightEnd)
        {
            //put smaller into temp array, move pointers forward
            if(arr[leftStart].compareTo(arr[rightStart]) <= 0)
            {
                temp[tempPos] = arr[leftStart];
                tempPos++;
                leftStart++;
            }
            else
            {
                temp[tempPos] = arr[rightStart];
                tempPos++;
                rightStart++;
            }
        }

        //while items left in either list
        while(leftStart <= leftEnd)
        {
            //add left over items to end of temp array
            temp[tempPos] = arr[leftStart];
            tempPos++;
            leftStart++;
        }
        while(rightStart <= rightEnd)
        {
            //add left over items to end of temp array
            temp[tempPos] = arr[rightStart];
            tempPos++;
            rightStart++;
        }


        //merge temp data to original using number of items and rightEnd
        for(int i = 0; i < numberElements; i++,rightEnd--)
        {
            arr[rightEnd] = temp[rightEnd];
        }

        System.out.println("End Merge: " + numberElements + " :: " + Arrays.toString(Arrays.copyOfRange(arr, startPrint, endPrint+1)));
    }


    public static void radixSortStringsNew (String[]arr){
       String [] aux = new String[arr.length];
        sortNew(arr, aux, 0, arr.length - 1, 0);
    }
    private static void sortNew(String[] a, String[] aux, int lo, int hi, int d)
    {
        /*
        if (hi <= lo) return;
        int[] count = new int[R+2];
        for (int i = lo; i <= hi; i++)
            count[charAt(a[i], d) + 2]++;
        for (int r = 0; r < R+1; r++)
            count[r+1] += count[r];
        for (int i = lo; i <= hi; i++)
            aux[count[charAt(a[i], d) + 1]++] = a[i];
        for (int i = lo; i <= hi; i++)
            a[i] = aux[i - lo];
        for (int r = 0; r < R; r++)
            sortNew(a, aux, lo + count[r], lo + count[r+1] - 1, d+1);
            */
        }
    private static void sortVariable (String []a){
    }

    public static void radixSortStrings(String[] arr, int stringLen)
    {
        //number of buckets = 256 (characters in character set)
        final int BUCKETS = 256;

        //create buckets
        ArrayList<String>[] buckets = (ArrayList<String>[])new ArrayList[BUCKETS];
        //initialize array of ArrayLists
        for(int i = 0; i < BUCKETS; i++)
        {
            buckets[i] = new ArrayList<String>();
        }

        //loop from end of string to beginning
        for(int pos = stringLen -1; pos >= 0; pos--)
        {
            //loop through strings
            for(String s : arr)
            {
                //add to bucket based on character at position
                System.out.println("Buckets Pos : " + s.charAt(pos));
                System.out.println("Current String : " + s.charAt(pos));
                buckets[s.charAt(pos)].add(s);
            }

            int index = 0;
            //loop through buckets
            for(ArrayList<String> singleBucket : buckets)
            {
                //add each string back to original array
                for(String s : singleBucket)
                {
                    arr[index] = s;
                    index++;
                }
                //clear bucket
                singleBucket.clear();
            }
            System.out.println(pos + " : " + Arrays.toString(arr));
        }
    }
}

