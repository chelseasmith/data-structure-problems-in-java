package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class RadixSortVariable is built using the same type of sorting as our example in class but allows for
 * a user to input strings instead of statically define them and allows for as much input as you like. this class
 * will allow sort strings. This Sorts strings based off the charAt method and sorts them by the chars ASCII value
 * @author Chelsea Smith
 * @version 1.0,7/14/15
 * @since  1.0,7/14/15
 * @see Scanner
 * @see String
 * @see Character
 * @see #charAt(String, int)
 * @see #sort(String[]) public method.
 * @see #sort(String[], int, int, int, String[]) class private method for recursively sorting the String[] array.
 * STEP 1 - (See main in this class) - init scanner and arraylist, run a while loop to take in user input for all the
 * strings the user wishes to run the RadixSort on. enter q to quit.
 * STEP 2 - (see public static void sort(String[]a)) the public sort method is called, which computes N(the length of the array)
 * and from there creates a new array to hold the new sorted array and then calls the private recursive method
 * sort(String[] a, int lo, int hi, int d, String[] aux), see method defintion for details.
 * STEP 3 - inside the private sort method, we first check to see if the hi is less than or equal to the lo+CUTOFF(15),
 * if it is time to call the insertion() method to and return the void function with the sorted array.
 * STEP 4 - the insertion(String[] a, int lo, int hi, int d) does insertion sort a[lo] to a[hi], starting at dth character,
 * this method checks to see if the current string and the string before it and use the less(String v, String w, int d)
 * which checks the ASCII value it will loop through
 * starting with the dth position and stop a the lo to check to see which character's ASCII value is less. if the given string's characterAt(d)
 * is the same as the other, the loop will continue, if the given string's ASCII value a pos d is less then the other strings ASCII
 * value at pos d the method will return true, and will return false if the previous condition is vice versa.
 * STEP 5 - once the less condition is true the exch(int i ,int j
 */

public class RadixSortVariable {
    private static final int BITS_PER_BYTE =   8;
    private static final int BITS_PER_INT  =  32;
    private static final int R             = 256; // SIZE of ASCII characters
    private static final int CUTOFF        =  15; // CUTOFF for the insertion method.

    /**
     * this is the static public method you call to sort the array using a RadixSort for variable Strings lengths and inputs.
     * @param a the string array you wish to sort
     * @return void but sorts the array you have inputed.
     */
    public static void sort(String[] a) {
        int N = a.length;
        String[] aux = new String[N];
        sort(a, 0, N-1, 0, aux);
    }

    // return dth character of s, -1 if d = length of string
    private static int charAt(String s, int d) {
        assert d >= 0 && d <= s.length();
        if (d == s.length()) return -1;
        return s.charAt(d);
    }

    // sort from a[lo] to a[hi], starting at the dth character

    /**
     * this recursive method is used to recursively sort the input string array.
     * @param a - the original array unsorted.
     * @param lo - the low bound of the sorted array that has not been yet sorted, if first called is Zero
     * @param hi - the high bound of the sorted array that has been yet sorted, if first called is Array.length-1
     * @param d - the  index of character to start the sorting at in, if first called is 0
     * @param aux - the array that is used to switch around the index for output while in process of the sort.
     */
    private static void sort(String[] a, int lo, int hi, int d, String[] aux) {

        // cutoff to insertion sort for small subarrays
        if (hi <= lo + CUTOFF) {
            insertion(a, lo, hi, d);
            return;
        }

        // compute frequency counts
        int[] count = new int[R+2];
        for (int i = lo; i <= hi; i++) {
            int c = charAt(a[i], d);
            count[c+2]++;
        }

        // transform counts to indicies
        for (int r = 0; r < R+1; r++)
            count[r+1] += count[r];

        // distribute
        for (int i = lo; i <= hi; i++) {
            int c = charAt(a[i], d);
            aux[count[c+1]++] = a[i];
        }

        // copy back
        for (int i = lo; i <= hi; i++)
            a[i] = aux[i - lo];


        // recursively sort for each character (excludes sentinel -1)
        for (int r = 0; r < R; r++)
            sort(a, lo + count[r], lo + count[r+1] - 1, d+1, aux);
    }


    // insertion sort a[lo..hi], starting at dth character
    private static void insertion(String[] a, int lo, int hi, int d) {
        for (int i = lo; i <= hi; i++)
            for (int j = i; j > lo && less(a[j], a[j-1], d); j--)
                exch(a, j, j-1);
    }

    // exchange a[i] and a[j]
    private static void exch(String[] a, int i, int j) {
        String temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    // is v less than w, starting at character d
    private static boolean less(String v, String w, int d) {
        // assert v.substring(0, d).equals(w.substring(0, d));
        for (int i = d; i < Math.min(v.length(), w.length()); i++) {
            if (v.charAt(i) < w.charAt(i)) return true;
            if (v.charAt(i) > w.charAt(i)) return false;
        }
        return v.length() < w.length();
    }
    public static void printOutput (String []sorted){
        System.out.println("********************************** String Array Sorted Output **********************************");
        for (int i = 0; i < sorted.length;i++){
            System.out.println("String a position " + i + " : " + sorted[i]);
        }
        System.out.println("********************************** String Array Sorted Output End **********************************");
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        List<String>list = new ArrayList<>();
        System.out.println("Enter a list of Strings. enter q to stop entering and sort the strings");
        String input = s.nextLine();
        while (!input.equalsIgnoreCase("q")){
           list.add(input);
           input = s.nextLine();
        }
        String[] a = new String[list.size()];
        list.toArray(a);
        int N = a.length;
        sort(a);
        printOutput(a);
    }

}
