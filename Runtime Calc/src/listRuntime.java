import java.util.ArrayList;
import java.util.List;

/**
 * Created by cjsmith1102 on 6/15/2015.
 */

public class listRuntime {

    public static void main (String[]args){
        long startTime = System.nanoTime();
        List list = makeList(10);
        long endTime = System.nanoTime();
        System.out.println(endTime-startTime);
    }

    public static List<Integer> makeList(int N) {
        ArrayList<Integer> lst = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            lst.add(i);
            lst.trimToSize();
        }
        return lst;
    }
}