/**
 * Created by cjsmith1102 on 6/14/2015.
 */
public class myStack<E> {
    Node<E> first;
    public myStack()
    {
        first = null;
    }
    public void push(E val)
    {
        Node<E> newData = new Node<E>(val);
        if(first == null)
            first = newData;
        else
        {
            newData.next = first;//set first node to come after newData
            first = newData;//set newData to be first node
        }
    }
    public E pop()
    {
        if(first == null)
            return null;
        else
        {
            E temp = first.val;//store first node value
            first = first.next;//overwrite first node with next node
            return temp;//return original first node value
        }
    }
    public E top()
    {
        if(first == null)
            return null;
        return first.val;//return first node value
    }
    public E peek()
    {
        return top();//alias for top() method
    }
    private class Node<F>
    {
        public Node<F> next;
        public F val;
        public Node(F v)
        {
            next = null;
            val = v;
        }
    }
}

