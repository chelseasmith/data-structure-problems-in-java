/**
 * Created by cjsmith1102 on 6/14/2015.
 */
import java.util.Scanner;


public class testMyStack {

    public static void main(String[] args) {
		/*
		myStack<String> strings = new myStack<String>();
		strings.push("!");
		strings.push("World");
		strings.push(", ");
		strings.push("Hello");
		while(strings.top() != null)
			System.out.print(strings.pop());
		*/
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a line of code: ");
        String temp = s.nextLine();
		/*
		 * Symbols to look for:
		 * [] {} () /* */ 						/*
		 */
        myStack<String> symbols = new myStack<String>();
        boolean balanced = true;
        while(temp.length() > 0 && balanced)
        {
            switch(temp.charAt(0))
            {
                case '[':
                case '(':
                case '{':
                    symbols.push(temp.substring(0,1));
                    temp = temp.substring(1);
                    break;
                case '/':
                    if(temp.length() > 1 && temp.charAt(1) == '*')
                    {
                        symbols.push(temp.substring(0,2));
                        temp = temp.substring(2);
                    }
                    break;
                case '*':
                    if(temp.length() > 1 && temp.charAt(1) == '/')
                    {
                        if(symbols.pop().equals("/*"))
                        {
                            temp = temp.substring(2);
                        }
                        else
                            balanced = false;
                    }
                    break;
                case ']':
                    if(symbols.pop().equals("["))
                    {
                        temp = temp.substring(1);
                    }
                    else
                        balanced = false;
                    break;
                case ')':
                    if(symbols.pop().equals("("))
                    {
                        temp = temp.substring(1);
                    }
                    else
                        balanced = false;
                    break;
                case '}':
                    if(symbols.pop().equals("{"))
                    {
                        temp = temp.substring(1);
                    }
                    else
                        balanced = false;
                    break;
                default:
                    temp = temp.substring(1);
                    break;
            }
        }
        if(balanced)
            System.out.println("Code is balanced!");
        else
            System.out.println("Code is not balanced!");
    }

}

