4.2 For each node in the tree of Figure 4.70:
a. Name the parent node.
 A
b. List the children. 
B, C, D, E, F, G, H, I, J, L, M, & K
c. List the siblings.
A: B & C 
B: D & E
D: G & H
E: I & J
J: L & M
d. Compute the depth.
3
e. Compute the height.
5
4.9 
a. Show the result of inserting 3, 1, 4, 6, 9, 2, 5, 7 into an initially empty 
binary search tree.
    public static void main(String[]args) throws Exception {
        BinarySearchTree<Integer> t = new BinarySearchTree<>();
        // 3, 1, 4, 6, 9, 2, 5, 7
        t.insert(3);
        t.insert(1);
        t.insert(4);
        t.insert(6);
        t.insert(9);
        t.insert(2);
        t.insert(5);
        t.insert(7);
        t.printTree();
1
2
3
4
5
6
7
9
*Shows ordered list
*/897
b. Show the result of deleting the root.
    public static void main(String[]args) throws Exception {
        BinarySearchTree<Integer> t = new BinarySearchTree<>();
        // 3, 1, 4, 6, 9, 2, 5, 7
        t.insert(3);
        t.insert(1);
        t.insert(4);
        t.insert(6);
        t.insert(9);
        t.insert(2);
        t.insert(5);
        t.insert(7);
        t.remove(t.root.element);
        t.printTree();
1
2
4
5
6
7
9
*Shows ordered list minus the root which was 3

*Check: FolderList.java

Using the linked list implementation for a general tree, create a java program to store a folder 
structure in a tree, then print the tree. Include a space for each depth the folder is in the tree.

File f = new File("P:\\");
if(f.isDirectory())
  for(File sub : f.listFiles())

*Check: EquationTreeTester.java

Expand on the example we did in class of storing infix math equations as a binary tree. 
Allow the user to supply postfix/prefix versions of the equation (ask which type, then pass to the 
correct method to put into the tree). Store the equation into the tree and print all three versions 
of the equation from the tree.