import java.util.Scanner;

public class EquationTreeTester {
    private static String choice = "";
    private static EquationTree t = new EquationTree();
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String userChoice = getChoice(s);
        switch (userChoice){
            case "infix":
                infix(s);
                break;
            case "postfix":
                postfix(s);
                break;
            case "prefix":
                prefix(s);
                break;
            default:
                System.out.println("Error something went wrong!");
                break;
        }
    }
    public static String getChoice (Scanner s){
        System.out.println("are you inputting infix, postfix, or prefix expression");
        choice = s.nextLine();
        while (!choice.equalsIgnoreCase("infix") && !choice.equalsIgnoreCase("postfix") && !choice.equalsIgnoreCase("prefix")){
            System.out.println("Error! you did not enter correct expression remember you must enter infix, postfix, or prefix");
           choice = s.nextLine();
        }
        return choice.toLowerCase();
    }
    public static void infix (Scanner s){
        System.out.print("Enter an infix format equation: ");
        String infix = s.nextLine();
        t.populateTreeFromInfix(infix);
        System.out.println(t.infixPrinter());
        System.out.println(t.postfixPrinter());
        System.out.println(t.prefixPrinter());
    }
    public static boolean checkInput (String input){
        for (int i = 0; i < input.length();i++){
            char c = input.charAt(i);
            if (i ==0){
                if(c == ' '){return false;}
            }
            else if (i % 2 == 1){
                if (c != ' '){return false;}
            }
            else if (i % 2 == 0){
                if (c==' '){return false;}
            }
        }
        return true;
    }
    public static void postfix (Scanner s){
        System.out.print("Enter an postfix format equation with spaces between each input: ");
        String input = s.nextLine();
        while (!checkInput(input)){
            System.out.println("Error! please enter postfix equation with spaces between each input: ");
            input = s.nextLine();
        }


        ExpressionEvaluator e = new ExpressionEvaluator(input, ExpressionEvaluator.EXPRESSIONTYPE.Postfix);
        String infix = e.GetInfixExpression().trim();
        infix = infix.replace(" ","");
        t.populateTreeFromInfix(infix);
        System.out.println(t.infixPrinter());
        System.out.println(t.postfixPrinter());
        System.out.println(t.prefixPrinter());
    }
    public static void prefix (Scanner s){
        System.out.print("Enter an prefix format equation with spaces between each input: ");
        String input = s.nextLine();
        while (!checkInput(input)){
            System.out.println("Error! please enter a prefix equation with spaces between each input: ");
            input = s.nextLine();
        }
        ExpressionEvaluator e = new ExpressionEvaluator(input,ExpressionEvaluator.EXPRESSIONTYPE.Prefix);
        String infix =e.GetInfixExpression().trim();
        infix = infix.replace(" ","");
        t.populateTreeFromInfix(infix);
        System.out.println(t.infixPrinter());
        System.out.println(t.postfixPrinter());
        System.out.println(t.prefixPrinter());
    }

}