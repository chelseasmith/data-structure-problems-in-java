import java.io.File;
import java.util.LinkedList;
public class FolderList  {
    private LinkedList<File>directories;
    private File topFile = null;
    private File bottomFile = null;
    private static int COUNT = 0;

    public FolderList (){
        directories = new LinkedList<File>();
    }
    public void add (File file){
        if (this.isEmpty() && file!=null){
            directories.add(file);
            topFile = file;
        }
        else if (file!=null){
            directories.add(file);
        }
        else{
            return;
        }
    }
    private boolean isEmpty(){
        return directories.size() > 0 ? true : false;
    }
    private String getSpaces (int sp){
        String spaces = "";
        if (sp == 0){
            return spaces;
        }
        for (int i = 0 ; i < sp; i++){
            spaces+="  ";
        }
        return spaces;
    }
    private String printFolderStructure (){
        if (directories.size() > 0){
            return getRecursiveFilePaths((directories.toArray(new File[directories.size()])),new StringBuilder(""),0).toString();
        }
        return this.topFile.getName();
    }
    public static void printFileCount (){
        System.out.println("NUMBER OF FILES TREE RAN THROUGH : " + COUNT);
    }
    private StringBuilder getRecursiveFilePaths (File[]files,StringBuilder output,int spaces){
        for (int i = 0; i < files.length;i++){
            if (files[i].isDirectory()){
                output.append(getSpaces(spaces)+files[i].getName() + "\n");
                COUNT++;
               output =  this.getRecursiveFilePaths(files[i].listFiles(), new StringBuilder(output.toString()), spaces + 1);
            }
            else{
                output.append(getSpaces(spaces)+files[i].getName() + "\n");
                COUNT++;
            }
        }
        return output;
    }
    public String toString (){
        return printFolderStructure();
    }



}
