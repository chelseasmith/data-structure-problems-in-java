/*
 * Name: Chelsea Smith
 * Class: CSCI 3200
 * Assignment 5: Using the linked list implementation for a general tree, create a java program to store a folder
 * structure in a tree, then print the tree. Include a space for each depth the folder is in the tree.
File f = new File("P:\\");
if(f.isDirectory())
  for(File sub : f.listFiles())
 * Date: 06/27/2015
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class BinaryTree {

    private static ArrayList<String> paths = new ArrayList<String>();
    private static String tree = "";
    private static String root = "";

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //File f = new File("P:\\");
        //if(f.isDirectory())
        //    for(File sub : f.listFiles())
        tree = scan.nextLine();//correct format : (a()())
        String[] t = splitTree(tree);
        //System.out.println(Arrays.toString(t));
        if (t.length == 1)
        {
            System.out.println(t[0]);
        }
        else{
            root = t[0];
            for (int i = 1; i < t.length;i++)
            {
                path("", t[i],0);
            }
            printPaths();
        }

    }
    public static String getPath (String current,String top)
    {
        String output = top;
        for (int i = 0; i < current.length();i++)
        {
            char c = current.charAt(i);
            if (c!='(' && c!=')')
            {
                output+=c;
            }
        }
        return output;
    }
    public static void printPaths (){
        System.out.println("************************* PATHS FOR TREE *************************");
        for (int i = 0; i <paths.size();i++)
        {
            System.out.println("PATH " + i + " : "+ root + paths.get(i));
        }
        System.out.println("************************* END OF PATHS *************************");
    }

    // 65 to 122
    public static boolean path(String currentPath,String tree,int parenCount)
    {
        if(parenCount < currentPath.length())
        {
            currentPath = currentPath.substring(0, currentPath.length()-1);
        }
        if (tree.length()==0)
        {
            return true;
        }

        if (isLetter(tree.charAt(0)))
        {
            if (isEnd(tree))
            {
                paths.add(currentPath + tree.charAt(0));
            }
            else{
                currentPath+=tree.charAt(0);
            }
        }
        else{
            if (tree.charAt(0)== '(')
            {
                parenCount++;
            }
            else if (tree.charAt(0)==')')
            {
                parenCount--;
            }
        }
        return path(currentPath,tree.substring(1),parenCount);
        //look at tree
        //if letter, check for 2 empty children
        //if 2 empty children, add full path to ArrayList
        //if not to empty children, run method again for left/right children with extra letter added to path
        //Notes:
        //tree.charAt(index);
        //tree = tree.substring(start);//returns everything after starting point
    }
    public static void pathAlt(String currentPath, String tree){}
    //expects (character tree tree)
    //returns [0] char; [1] left tree; [2] right tree;
    public static String[] splitTree(String tree)
    {
        //expected format
        //(node tree tree)
        //0 1 2-x x-(length-2) length-1
        if(tree.length() <= 2)
            return new String[]{tree};

        String[] temp = new String[3];
        temp[0] = "" + tree.charAt(1);
        tree = tree.substring(2, tree.length()-1);
        int parenCount = 0;
        int endTreeOne = 0;
        for(int i = 0; i < tree.length(); i++)
        {
            if(tree.charAt(i) == '(')
                parenCount++;
            if(tree.charAt(i) == ')')
                parenCount--;
            if(parenCount == 0)
            {
                //System.out.println("got into break Tree!");
                endTreeOne = i;
                break;
            }
        }
        temp[1] = tree.substring(0, endTreeOne+1);//left tree
        temp[2] = tree.substring(endTreeOne+1);//right tree
        //System.out.println(tree.substring(0, endTreeOne+1));
        //System.out.println(tree.substring(endTreeOne+1));

        return temp;
    }
    public static boolean isLetter (char in)
    {
        int i = (int)in;
        if (i >=65 && i<=122)
        {
            return true;
        }
        return false;
    }
    public static boolean isEnd (String tree)
    {
        boolean isPath = true;
        for (int i =1; i < 5; i++)
        {
            if (tree.charAt(i)!='(' && tree.charAt(i)!=')')
            {
                isPath = false;
            }
        }
        return isPath;
    }

}