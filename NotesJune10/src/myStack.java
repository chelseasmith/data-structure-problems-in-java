/**
 * Created by cjsmith1102 on 6/10/2015.
 */
public class myStack {
    Node<E> first;
    public myStack(){
        first = null;
    }
    public void push (E val)
    {
        Node<E> newData = new Node<E>(val);
        if(first == null)
            first = newData;
        else
        {
            newData.next=first;
            first = newData;
        }
    }
    public E pop()
    {
        if(first == null)
            return null;
        else
        {
            E temp = first.val;
            first = first.next;
            return temp;

        }
    }
    public E top()
    {
        if(first == null)
            return null;
        return first.val;
    }
    public E peek()
    {
        return top();
    }
    private class Node<F>
    {
        public Node<F> nexxt;
        public F val;
        public Node(F v)
    }
}
