/**
 * Created by cjsmith1102 on 6/17/2015.
 */

import java.util.Scanner;
import java.util.Stack;
public class PostfixToInfix {

        private static boolean isOperator(char c){
            if(c == '+' || c == '-' || c == '*' || c =='%' || c == '^')
                return true;
            return false;
        }
        public static  String convert(String postfix){
            MyStack<String> s = new MyStack<>();

            for(int i = 0; i < postfix.length(); i++){
                char c = postfix.charAt(i);
                if(isOperator(c)){
                    String b = s.pop();
                    String a = s.pop();
                    s.push("("+a+c+b+")");
                   // s.push(""+a+c+b+"");
                }
                else
                    s.push(""+c);
            }

            return s.pop();
        }
    }
