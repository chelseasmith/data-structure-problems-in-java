import java.util.LinkedList;

/**
 * Name: Chelsea Smith
 * Class: CSCI 3200
 * Professor: JET
 * Assignment: Create a linked list version of a QueueADT (myQueue) Implement it to allow for generic types (just like
 * myStack that we did in class). Store a first/last node to allow for easy constant time methods. Write a short
 * testMyQueue class to test your Queue
 * Date: 06/17/2015
 */

public class myQueue<E>  {
    public Node first;
    public Node last;
    private LinkedList<E>list = new LinkedList<E>();
    public myQueue()
    {
        first = null;
        list = new LinkedList();
        last = null;
    }

    /**
     * enqueue - add E to the end of the list.
     * @param item
     */
    public void enqueue(E item) {
        Node newData = new Node<E>(item);
        list.add((E) newData);
        if (list.size() == 1) {
            first = newData;
            last = newData;
        }
        else if (list.size() == 0){
            first = null;
            last = null;
        }
        else{
            last = newData;
        }
    }

    public boolean isEmpty()
    {
        return ((list.size() == 0) ? true : false);
    }
   // removes the first element in the list
    private void dequeue()
    {
        if (!this.isEmpty()){
            try {
                Node f = (Node) list.get(0);
                if (f!=null && f.val!=null){
                    list.remove(0);
                }
                else{
                    System.out.println("Cannot dequeue a list with null Node");
                }
            }
            catch (Exception e){
                System.out.println("cannot dequeue a list with a null Node or a size of zero");
            }
        }
        else{
            System.out.println("Cannot dequeue a list that has not Nodes.");
        }
        if (list.isEmpty()){
            first = null;
            last = null;
        }
        else if (list.size() == 1){
            first = (Node)list.get(0);
            last = (Node)list.get(0);
        }
        else{
            first = (Node)list.get(0);
            last = (Node)list.get(list.size()-1);
        }
    }
    public E peek(){
        if (!this.isEmpty()){
            if (first!=null) {
                Node temp = first;
                this.dequeue();
                return (E)temp.val;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }

    private class Node<F>
    {
        public Node<F> next;
        public F val;
        public Node(F v)
        {
            next = null;
            val = v;
        }
    }
}

