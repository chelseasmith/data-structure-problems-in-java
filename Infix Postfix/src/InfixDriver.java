import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by cjsmith1102 on 6/17/2015.
 */
public class InfixDriver {
    public static void main(String[] args) {
        // look in this class to see me use MyStack
        InfixToPostfixParens parens = new InfixToPostfixParens();
        String infixToPostfix = "";
        String postfixtoInfix  = "";
        String choice = "";
        boolean error = false;
        Scanner s = new Scanner (System.in);
        System.out.println("is the expression infix or postfix?");
        choice = s.nextLine();
        while (choice.equalsIgnoreCase("infix")==false && choice.equalsIgnoreCase("postfix")==false){
            System.out.println("Error! please enter infix or postfix");
            choice = s.nextLine();
        }
        if (choice.equalsIgnoreCase("infix")){
            System.out.println("Input the infix expression (operands, operators are separated by spaces");
            String inp = s.nextLine();
            while (inp.length()  <1){
                System.out.println("input length must be greater than zero");
                System.out.println("Input the infix expression (operands, operators are separated by spaces");
                inp = s.nextLine();
            }
            infixToPostfix(inp);
        }
        else if (choice.equalsIgnoreCase("postfix")){
            System.out.println("enter postfix expression with no spaces");
            String inp = s.nextLine();
            while (inp.length()  <1){
                System.out.println("input length must be greater than zero");
                System.out.println("enter postfix expression with no spaces");
                inp = s.nextLine();
            }
            postfixToInfix(inp);
        }
        else{
            System.out.println("an error occured. goodbye");
        }
    }
    public static void infixToPostfix(String inp){
        boolean error = false;
        InfixToPostfixParens parens = new InfixToPostfixParens();
        String infixToPostfix = "";
        try (Scanner input = new Scanner(System.in)) {
                try {
                    infixToPostfix = parens.convert(inp);
                    if (!infixToPostfix.equalsIgnoreCase("")){
                        error = false;
                    }
                    else{
                        error = true;
                    }
                    if (!error){
                        System.out.println("Converted postfix expression: " + infixToPostfix);
                        System.out.println("oringial infix expression " + inp);
                    }
                    else{
                        System.out.println("error occured goodbye");
                    }
                } catch (Exception e) {
                    System.out.println("error occured goodbye");
                }
            }
        }
    public static void postfixToInfix(String input){
        System.out.println("infix expression : " + PostfixToInfix.convert(input));
        System.out.println("postfix expression oringially inputted : " + input);

    }
    }
