import java.util.*;

/**
 * Created by cjsmith1102 on 6/17/2015.
 */

public class InfixToPostfixParens {

    public static class SyntaxErrorException
            extends Exception {
        SyntaxErrorException(String message) {
            super(message);
        }
    }

    private MyStack <Character> operatorStack;

    private static final String OPERATORS = "+-*%()^";
    private static final int[] PRECEDENCE = {
            1, 1, 2, 2, -1, -1,-2};

    private StringBuilder postfix;

    public  String convert(String infix) throws SyntaxErrorException {
        operatorStack = new MyStack < Character > ();
        postfix = new StringBuilder();
        StringTokenizer infixTokens = new StringTokenizer(infix);
        try {
            // Process each token in the infix string.
            while (infixTokens.hasMoreTokens()) {
                String nextToken = infixTokens.nextToken();
                char firstChar = nextToken.charAt(0);
                // Is it an operand?
                if (Character.isJavaIdentifierStart(firstChar)
                        || Character.isDigit(firstChar)) {
                    postfix.append(nextToken);
                    postfix.append(' ');
                } // Is it an operator?
                else if (isOperator(firstChar)) {
                    processOperator(firstChar);
                }
                else {
                    System.out.println("Throwing unexpected syntax exception") ;
                    throw new SyntaxErrorException
                            ("Unexpected Character Encountered: "
                                    + firstChar);
                }
            }
            while (!operatorStack.empty()) {
                char op = operatorStack.pop();
                    // Any '(' on the stack is not matched.
                    if (op == '(') {
                        System.out.println("unmatched opening parens");
                        throw new SyntaxErrorException(
                                "Unmatched opening parenthesis");
                    }
                    postfix.append(op);
                    postfix.append(' ');
                }
            return postfix.toString();
        }
        catch (EmptyStackException ex) {
            System.out.println("the stack is emtpy!!!");
            throw new SyntaxErrorException
                    ("Syntax Error: The stack is empty");
        }
    }



    private void processOperator(char op) {
        if (operatorStack.empty() || op == '(') {
            operatorStack.push(op);
        }
        else {
            char topOp = operatorStack.peek();
            if (precedence(op) > precedence(topOp)) {
                operatorStack.push(op);
            }
            else {
                while (!operatorStack.empty()
                        && precedence(op) <= precedence(topOp)) {
                    operatorStack.pop();
                    if (topOp == '(') {
                        // Matching '(' popped - exit loop.
                        break;
                    }
                    postfix.append(topOp);
                    postfix.append(' ');
                    if (!operatorStack.empty()) {
                        // Reset topOp.
                        topOp = operatorStack.peek();
                    }
                }
                if (op != ')')
                    operatorStack.push(op);
            }
        }
    }
    private boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != -1;
    }
    private int precedence(char op) {
        return PRECEDENCE[OPERATORS.indexOf(op)];
    }
 }