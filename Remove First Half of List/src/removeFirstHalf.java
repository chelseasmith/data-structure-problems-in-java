import java.util.List;

/**
 * Created by cjsmith1102 on 6/15/2015.
 */
public class removeFirstHalf {

    public static void removeFirstHalf( List<?> lst )
    {
        int theSize = lst.size( ) / 2;
        for( int i = 0; i < theSize; i++ )
            lst.remove( 0 );
    }
}
