import java.util.Random;

/**
 *Chelsea Smith
 * Assignment 6
 */

public class LazySearchTreeDriver {
    public static void main (String[]args){
         BinarySearchTree tree = new BinarySearchTree();
        Random r = new Random();
        int min = 0;
        int max = 0;
        int x = 0;
        while (x < 99){
            int insertInt = r.nextInt(100)+x;
            if (x==0){
                min = insertInt;
                max = insertInt;
            }
            else{
                if (insertInt < min){
                    min = insertInt;
                }
                else if (insertInt > max){
                    max = insertInt;
                }
            }
            tree.insert(insertInt);
            ++x;
        }
        tree.printTree();
        tree.remove(min);
        tree.remove(max);
        System.out.println("------- WE JUST DELETED THE MAX and MIN!!!!!!!");
        tree.printTree();
        try {
            System.out.println("THE MIN : " + tree.findMin());
        }
        catch (Exception e){
            System.out.println("ERROR FINDING MIN!");
        }
        try{
            System.out.println("THE MAX : " + tree.findMax());
        }
        catch (Exception e){
            System.out.println("ERROR FINDING MAX!");
        }


    }
}
