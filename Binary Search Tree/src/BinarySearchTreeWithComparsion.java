// BinarySearchTree class
//
// CONSTRUCTION: with no initializer
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x )       --> Insert x
// void remove( x )       --> Remove x
// boolean contains( x )  --> Return true if x is present
// Comparable findMin( )  --> Return smallest item
// Comparable findMax( )  --> Return largest item
// boolean isEmpty( )     --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void printTree( )      --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as appropriate

import java.util.ArrayList;

/**
 *Chelsea Smith
 * Assignment 6
 * 4.46 Two binary trees are similar if they are both empty or both nonempty and have similar left and right subtrees.
 * Write a method to decide whether two binary trees are similar. What is the running time of your method?
 */
public class BinarySearchTreeWithComparsion<AnyType extends Comparable<? super AnyType>>
{
    /**
     * Construct the tree.
     */
    public BinarySearchTreeWithComparsion( )
    {
        root = null;
    }

    /**
     * Insert into the tree; duplicates are ignored.
     * @param x the item to insert.
     */
    public void insert( AnyType x )
    {
        root = insert( x, root );
    }

    /**
     * Remove from the tree. Nothing is done if x is not found.
     * @param x the item to remove.
     */
    public void remove( AnyType x )
    {
        root = remove(x, root);
    }

    /**
     * Find the smallest item in the tree.
     * @return smallest item or null if empty.
     */
    public AnyType findMin( ) throws Exception {
        if( isEmpty( ) )
            throw new Exception( );
        return findMin( root ).element;
    }

    /**
     * Find the largest item in the tree.
     * @return the largest item of null if empty.
     */
    public AnyType findMax( ) throws Exception {
        if( isEmpty( ) )
            throw new Exception();
        return findMax(root).element;
    }
    private ArrayList<AnyType> getNodeValues() {
        ArrayList<AnyType> anyTypes = new ArrayList<AnyType>();
        if (root != null){
            if (root.element != null) {
                BinaryNode node = root;
                boolean isElement = true;
                while (isElement) {

                    anyTypes.add((AnyType) node.element);
                    if (node.right == null) {
                        isElement = false;
                    } else {
                        node = node.right;
                    }
                }
            }
    }
        return anyTypes;
    }

    /**
     * Find an item in the tree.
     * @param x the item to search for.
     * @return true if not found.
     */
    public boolean contains( AnyType x )
    {
        return contains( x, root );
    }

    /**
     * Make the tree logically empty.
     */
    public void makeEmpty( )
    {
        root = null;
    }

    /**
     * Test if the tree is logically empty.
     * @return true if empty, false otherwise.
     */
    public boolean isEmpty( )
    {
        return root == null;
    }

    /**
     * Print the tree contents in sorted order.
     */
    public void printTree( )
    {
        if( isEmpty( ) )
            System.out.println( "Empty tree" );
        else
            printTree( root );
    }
    public boolean treesEqual (BinarySearchTreeWithComparsion other) throws Exception {
        if (this.isEmpty() && other.isEmpty()){
            return true;
        }
        else if (!this.isEmpty() && other.isEmpty()){
            return false;
        }
        else if (this.isEmpty() && !other.isEmpty()){
            return false;
        }
        else if (other.findMin() != this.findMin()){
            return false;
        }
        else if (other.findMax() != this.findMax()){
            return false;
        }
        else if (!this.root.equals(other.root)){
            return false;
        }
        return true;
    }

    /**
     * Internal method to insert into a subtree.
     * @param x the item to insert.
     * @param t the node that roots the subtree.
     * @return the new root of the subtree.
     */
    private BinaryNode<AnyType> insert( AnyType x, BinaryNode<AnyType> t )
    {
        if( t == null )
            return new BinaryNode<>( x, null, null );

        int compareResult = x.compareTo( t.element );

        if( compareResult < 0 )
            t.left = insert( x, t.left );
        else if( compareResult > 0 )
            t.right = insert( x, t.right );
        else
            ;  // Duplicate; do nothing
        return t;
    }

    /**
     * Internal method to remove from a subtree.
     * @param x the item to remove.
     * @param t the node that roots the subtree.
     * @return the new root of the subtree.
     */
    private BinaryNode<AnyType> remove( AnyType x, BinaryNode<AnyType> t )
    {
        if( t == null )
            return t;   // Item not found; do nothing

        int compareResult = x.compareTo( t.element );

        if( compareResult < 0 )
            t.left = remove( x, t.left );
        else if( compareResult > 0 )
            t.right = remove( x, t.right );
        else if( t.left != null && t.right != null ) // Two children
        {
            t.element = findMin( t.right ).element;
            t.right = remove( t.element, t.right );
        }
        else
            t = ( t.left != null ) ? t.left : t.right;
        return t;
    }

    /**
     * Internal method to find the smallest item in a subtree.
     * @param t the node that roots the subtree.
     * @return node containing the smallest item.
     */
    private BinaryNode<AnyType> findMin( BinaryNode<AnyType> t )
    {
        if( t == null )
            return null;
        else if( t.left == null )
            return t;
        return findMin( t.left );
    }

    /**
     * Internal method to find the largest item in a subtree.
     * @param t the node that roots the subtree.
     * @return node containing the largest item.
     */
    private BinaryNode<AnyType> findMax( BinaryNode<AnyType> t )
    {
        if( t != null )
            while( t.right != null )
                t = t.right;

        return t;
    }

    /**
     * Internal method to find an item in a subtree.
     * @param x is item to search for.
     * @param t the node that roots the subtree.
     * @return node containing the matched item.
     */
    private boolean contains( AnyType x, BinaryNode<AnyType> t )
    {
        if( t == null )
            return false;

        int compareResult = x.compareTo( t.element );

        if( compareResult < 0 )
            return contains( x, t.left );
        else if( compareResult > 0 )
            return contains( x, t.right );
        else
            return true;    // Match
    }

    /**
     * Internal method to print a subtree in sorted order.
     * @param t the node that roots the subtree.
     */
    private void printTree( BinaryNode<AnyType> t )
    {
        if( t != null )
        {
            printTree( t.left );
            System.out.println( t.element );
            printTree( t.right );
        }
    }

    /**
     * Internal method to compute height of a subtree.
     * @param t the node that roots the subtree.
     */
    private int height( BinaryNode<AnyType> t )
    {
        if( t == null )
            return -1;
        else
            return 1 + Math.max( height( t.left ), height( t.right ) );
    }

    // Basic node stored in unbalanced binary search trees
    private static class BinaryNode<AnyType>
    {
        // Constructors
        BinaryNode( AnyType theElement )
        {
            this( theElement, null, null );
        }

        BinaryNode( AnyType theElement, BinaryNode<AnyType> lt, BinaryNode<AnyType> rt )
        {
            element  = theElement;
            left     = lt;
            right    = rt;
        }
        public boolean equals (BinaryNode otherNode){
            if (this.element!=null && otherNode.element!=null) {
                if (this.element != otherNode.element) {
                    return false;
                }
            }
            else if (this.element == null && otherNode.element == null){

            }
            else{
                return false;
            }
            if (this.left !=null && otherNode.left !=null){
                if (!this.left.equals(otherNode.left)){
                    return false;
                }
            }
            else if (this.left == null && otherNode.left!=null){

            }
            else{
                return false;
            }
            if (this.right!=null && otherNode.right!=null) {
                if (!this.right.equals(otherNode.right)) {
                    return false;
                }
            }
            else if (this.right==null && otherNode.right ==null){

            }
            else{
                return false;
            }
            return true;
        }

        AnyType element;            // The data in the node
        BinaryNode<AnyType> left;   // Left child
        BinaryNode<AnyType> right;  // Right child
    }


    /** The tree root. */
    private BinaryNode<AnyType> root;


    // Test program
    public static void main( String [ ] args ) throws Exception {
        BinarySearchTreeWithComparsion<Integer> t = new BinarySearchTreeWithComparsion<>( );
        BinarySearchTreeWithComparsion<Integer>s = new BinarySearchTreeWithComparsion<>();
        if (t.getNodeValues().equals(s.getNodeValues())){
            System.out.println("Trees are equals because they are both empty");
        }
        t.insert(10);
        t.insert(20);
        s.insert(10);
        s.insert(20);
        if (t.getNodeValues().equals(s.getNodeValues())){
            System.out.println("are equal because they have the same min and max");
        }
        t.remove(20);
        if (t.getNodeValues().equals(s.getNodeValues())){
            System.out.println("we should not see this outputted because they are not equal");
        }
        else{
            System.out.println("in else case now because they are not equal");
        }
        t.remove(10);
        s.remove(10);
        s.remove(20);
        t = null;
        s = null;
        t = new BinarySearchTreeWithComparsion<Integer>();
        s = new BinarySearchTreeWithComparsion<Integer>();
        for (int i =0 ; i < 1000; i++){
            t.insert(i);
            s.insert(i);
        }
        long startTime = System.nanoTime();
        if (t.getNodeValues().equals(s.getNodeValues())){
            System.out.println("These are equal even after adding 1000 numbers to them");
        }
        long endTime = System.nanoTime();
        System.out.println("Time it took to calculate 1000 nodes were equal : " + (endTime-startTime));

        t = new BinarySearchTreeWithComparsion<>();
        s = new BinarySearchTreeWithComparsion<>();
        for (int i = 0 ; i < 10000; i++){
            t.insert(i);
            s.insert(i);
        }
        startTime = System.nanoTime();
        boolean isEqual = false;
        if (t.getNodeValues().equals(s.getNodeValues())){
            isEqual = true;
        }
        else{
            isEqual = false;
        }
        endTime = System.nanoTime();
        System.out.println("Result of comparing if 10000 nums equal : " + isEqual + ". Time it took to run this method : " + (endTime - startTime));
    }
}
