import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import javax.swing.text.html.parser.Entity;

/**
 * Name: Chelsea Smith
 * Class: CSCI 3200
 * Professor: JET
 * Assignment #6: 4.16, 4.19, 4.41, 4.46 (similar defined by same graphical structure, not by same values)
 * Write a program that asks a user for a list of unique names, put each name into a TreeMap, using the name as the key,
 * and the number of vowels in the name as the value. Traverse the map using an iterator to print out all the key/value pairs.
 * Hint: Ask the user for data in a while loop until a given String is entered, signaling no more names to enter.
 * Date: 06/29/2015
 */
public class NameTreeMap implements Iterator<String> {
    TreeMap<String,Integer> nameVals;
    private int pos;
    private String[]keys;

    public boolean nameExists (String name){
        return (nameVals.containsKey(name)) ? true : false;
    }
    public NameTreeMap (){
        nameVals = new TreeMap<>();
    }

    public void addName (String name){
        if (!nameExists(name)){
            nameVals.put(name,this.calculateVals(name));
            keys =  nameVals.keySet().toArray(new String[nameVals.size()]);
        }
        else{
            System.out.println("Error! this name has already been added!");
            return;
        }
    }
    private int calculateVals (String name){
        int vals = 0;
        for (int i = 0; i <  name.length(); i++){
            switch (name.charAt(i)){
                case 'a':
                case 'e':
                case'i':
                case 'o':
                case 'u':vals++;break;
                default:
                    break;
            }
        }
        return vals;
    }
    @Override
    public boolean hasNext() {return pos < nameVals.size() ? true : false;}

    @Override
    public String next() {
        int val = nameVals.get(keys[pos]);
        String key = keys[pos];
        pos++;
        return "Name : "+key + " number of vowels in name : " + val;
    }

    @Override
    public void remove() {
     // do nothing for now.
    }
}
