// BinarySearchTree class
//
// CONSTRUCTION: with no initializer
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x )       --> Insert x
// void remove( x )       --> Remove x
// boolean contains( x )  --> Return true if x is present
// Comparable findMin( )  --> Return smallest item
// Comparable findMax( )  --> Return largest item
// boolean isEmpty( )     --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void printTree( )      --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as appropriate

 /**
 *Chelsea Smith
 * Assignment 6
 * 4.46 Two binary trees are similar if they are both empty or both nonempty and have similar left and right subtrees.
 * Write a method to decide whether two binary trees are similar. What is the running time of your method?
 */

public class BinarySearchTree<AnyType extends Comparable<? super AnyType>> {
    private static  int DELETE_KEY = -1;
    private static BinaryNode TARD_NODE = new BinaryNode(DELETE_KEY);
    /**
     * Construct the tree.
     */
    public BinarySearchTree() {
        root = null;
    }

    /**
     * Insert into the tree; duplicates are ignored.
     *
     * @param x the item to insert.
     */
    public void insert(AnyType x) {
        root = insert(x, root);
    }

    /**
     * Remove from the tree. Nothing is done if x is not found.
     *
     * @param x the item to remove.
     */
    public void remove(AnyType x) {
        root = remove(x, root);
    }

    /**
     * Find the smallest item in the tree.
     *
     * @return smallest item or null if empty.
     */
    public AnyType findMin() throws Exception {
        if (this.isEmpty())
            throw new Exception();
        return findMin(root).element;
    }

    /**
     * Find the largest item in the tree.
     *
     * @return the largest item of null if empty.
     */
    public AnyType findMax() throws Exception {
        if (this.isEmpty())
            throw new Exception();
        return findMax(root).element;
    }

    /**
     * Find an item in the tree.
     *
     * @param x the item to search for.
     * @return true if not found.
     */
    public boolean contains(AnyType x) {
        return contains(x, root);
    }

    /**
     * Make the tree logically empty.
     */
    public void makeEmpty() {
        root = null;
    }

    /**
     * Test if the tree is logically empty.
     *
     * @return true if empty, false otherwise.
     */
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Print the tree contents in sorted order.
     */
    public void printTree() {
        if (isEmpty())
            System.out.println("Empty tree");
        else
            printTree(root);
    }

    /**
     * Internal method to insert into a subtree.
     *
     * @param x the item to insert.
     * @param t the node that roots the subtree.
     * @return the new root of the subtree.
     */
    private BinaryNode<AnyType> insert(AnyType x, BinaryNode<AnyType> t) {
        if (t == null)
            return new BinaryNode<>(x, null, null);
            if (t.element == TARD_NODE.element){
                if(t.left!=null){
                    int leftCompare = x.compareTo(t.left.element);
                    if (leftCompare < 0){
                        t.left = insert(x,t.left);
                    }
                }
                else if (t.right!=null){
                    int rightCompare =  x.compareTo(t.right.element);
                    if (rightCompare > 0){
                        t.right = insert(x,t.right);
                    }

                }
                t.element = x;
                return t;
            }
        int compareResult = x.compareTo(t.element);

        if (compareResult < 0) {
                    t.left = insert(x, t.left);

            }
        else if (compareResult > 0){
                t.right = insert(x,t.right);
            }
            ;  // Duplicate; do nothing
        return t;
    }

    /**
     * Internal method to remove from a subtree.
     *
     * @param x the item to remove.
     * @param t the node that roots the subtree.
     * @return the new root of the subtree.
     */
    private BinaryNode<AnyType> remove(AnyType x, BinaryNode<AnyType> t) {
        BinaryNode tempNode = null;
        if (t == null)
            return t;   // Item not found; do nothing

        int compareResult = x.compareTo(t.element);
        // we need to go lower in the tree
        if (compareResult < 0)
            t.left = remove(x, t.left);
            // we need to go higher in the tree
        else if (compareResult > 0)
            t.right = remove(x, t.right);
            // 2 children
        else {
            if (t.left != null && t.right != null) // Two children
            {
                t.element = findMin(t.right).element;
                t.right = remove(t.element, t.right);
            } else // equal
            {
                //t = (t.left != null) ? t.left : t.right;
                if (t.left!=null && t.right!=null){
                    tempNode = new BinaryNode(DELETE_KEY,t.left,t.right);
                }
                else if (t.left!=null){
                    tempNode = new BinaryNode(DELETE_KEY,t.left,null);
                }
                else if (t.right!=null){
                    tempNode = new BinaryNode(DELETE_KEY,null,t.right);
                }
                t = tempNode;
                // t = (t.left!=null) ? t.right =
            }
        }
        return t;
    }
    private boolean containsDumbyNodeLeft (BinaryNode node){
        if (node.left.element == TARD_NODE.element){
            return true;
        }
        return false;
    }
    private boolean containsDumbyNodeRight (BinaryNode node){
        if (node.right.element == TARD_NODE.element){
            return true;
        }
        return false;
    }
    private boolean containsDumbyNodeCenter (BinaryNode node){
        if (node.element == TARD_NODE.element){
            return true;
        }
        return false;
    }
    private BinaryNode findMinIfDumby (BinaryNode<AnyType>rNode){
        while (rNode.right!=null && rNode.right.element!=TARD_NODE.element){
            rNode = rNode.right;
        }

        return rNode;
    }
    private BinaryNode findMaxIfDumby (BinaryNode lNode){
        while (lNode.left!=null && lNode.left.element!=null){
            lNode = lNode.left;
        }
        return lNode;
    }
    /**
     * Internal method to find the smallest item in a subtree.
     *
     * @param t the node that roots the subtree.
     * @return node containing the smallest item.
     */
    private BinaryNode<AnyType> findMin(BinaryNode<AnyType> t) {
        boolean center = containsDumbyNodeCenter(t);
        if (t.element == TARD_NODE.element){
            t = findMinIfDumby(t);
        }
        if (t == null) {
            return null;
        }
        else if (t.left == null) {
            return t;
        }
        return findMin(t.left);
    }

    /**
     * Internal method to find the largest item in a subtree.
     *
     * @param t the node that roots the subtree.
     * @return node containing the largest item.
     */
    private BinaryNode<AnyType> findMax(BinaryNode<AnyType> t) {
        if (t != null)
            while (t.right != null) {
                t = t.right;
            }
        if (t.element == TARD_NODE.element){
           t =  findMaxIfDumby(t);
        }

        return t;
    }

    /**
     * Internal method to find an item in a subtree.
     *
     * @param x is item to search for.
     * @param t the node that roots the subtree.
     * @return node containing the matched item.
     */
    private boolean contains(AnyType x, BinaryNode<AnyType> t) {
        if (t == null)
            return false;

        int compareResult = x.compareTo(t.element);

        if (compareResult < 0)
            return contains(x, t.left);
        else if (compareResult > 0)
            return contains(x, t.right);
        else
            return true;    // Match
    }

    /**
     * Internal method to print a subtree in sorted order.
     *
     * @param t the node that roots the subtree.
     */
    private void printTree(BinaryNode<AnyType> t) {
        if (t != null) {
            printTree(t.left);
            if (t.element != TARD_NODE.element) {
                System.out.println(t.element);
            }
            printTree(t.right);
        }
    }

    /**
     * Internal method to compute height of a subtree.
     *
     * @param t the node that roots the subtree.
     */
    private int height(BinaryNode<AnyType> t) {
        if (t == null)
            return -1;
        else
            return 1 + Math.max(height(t.left), height(t.right));
    }

    // Basic node stored in unbalanced binary search trees
    private static class BinaryNode<AnyType> {
        // Constructors
        BinaryNode(AnyType theElement) {
            this(theElement, null, null);
        }

        BinaryNode(AnyType theElement, BinaryNode<AnyType> lt, BinaryNode<AnyType> rt) {
            element = theElement;
            left = lt;
            right = rt;
        }

        AnyType element;            // The data in the node
        BinaryNode<AnyType> left;   // Left child
        BinaryNode<AnyType> right;  // Right child
    }


    /**
     * The tree root.
     */
    private BinaryNode<AnyType> root;


    // Test program
    /*
    public static void main( String [ ] args )
    {

        BinarySearchTree<Integer> t = new BinarySearchTree<>( );
        final int NUMS = 4000;
        final int GAP  =   37;

        System.out.println( "Checking... (no more output means success)" );

        for( int i = GAP; i != 0; i = ( i + GAP ) % NUMS )
            t.insert( i );

        for( int i = 1; i < NUMS; i+= 2 )
            t.remove( i );

        if( NUMS < 40 )
            t.printTree( );
        if( this.findMin() != 2 || this.findMax( ) != NUMS - 2 )
            System.out.println( "FindMin or FindMax error!" );

        for( int i = 2; i < NUMS; i+=2 )
            if( !t.contains( i ) )
                System.out.println( "Find error1!" );

        for( int i = 1; i < NUMS; i+=2 )
        {
            if( t.contains( i ) )
                System.out.println( "Find error2!" );
        }
    }

}
*/
}