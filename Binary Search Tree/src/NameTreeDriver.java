import java.util.Iterator;
import java.util.Scanner;

/**
 *Chelsea Smith
 * Assignment 6
 */
public class NameTreeDriver {
    public static void main (String []args){
        NameTreeMap map = new NameTreeMap();
        String name = "";
        Scanner s = new Scanner(System.in);
        System.out.println("enter a name enter q to quit");
        name = s.nextLine();
        while (!name.equalsIgnoreCase("q")){
            map.addName(name);
            System.out.println("enter a name enter q to quit");
            name = s.nextLine();
        }
       while (map.hasNext()){
           System.out.println(map.next());
       }
    }
}
