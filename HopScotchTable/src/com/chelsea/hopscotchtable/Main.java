package com.chelsea.hopscotchtable;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        HashMap<Integer,Integer>map = initMap();
        HopScotchHashTable table = new HopScotchHashTable();
        table.putAll(map);
        table.put("hey", "there");
        table.put("blah", "spa");
        System.out.println(table);
        table.rehash();
        System.out.println(table);
        System.out.println(table.entrySet());
        table.clear();
        System.out.println(table);
        table.putAll(initMap());
        System.out.println(table);
        table.newTable(10000);
        table.put(new StringBuilder("bladklfjdlfjdklfjd"),new StringBuilder("djfdlkjfdlkjbl blgjdlfjdf ljfasldkfj \n"));
    }
    public static HashMap<Integer,Integer> initMap (){
        HashMap<Integer,Integer>map = new HashMap<>();
        Random r = new Random();
        int randomCount = r.nextInt(1000);
        if (randomCount < 5){
            r = new Random();
            randomCount = r.nextInt(1000);
        }
        for (int i = 0; i < randomCount;i++){
            map.put(i,((i+1) * (r.nextInt(100000)+1)));
        }
        return map;
    }
}
