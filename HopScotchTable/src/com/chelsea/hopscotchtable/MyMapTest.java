package com.chelsea.hopscotchtable;

/**
 * Created by will on 7/26/15.
 */
public class MyMapTest {
   public static void main (String []args){
       MyMap map = new MyMap();

       map.put(1.0023,"val");
       map.entrySet();
       System.out.println(map.get(1.0023));
       System.out.println(map.get(new Object()));
       map.put("key",23.3434);
       System.out.println(map.get("key"));
   }

}
