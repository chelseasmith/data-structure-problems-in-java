import java.util.LinkedList;
import java.util.List;

/**
 * Created by cjsmith1102 on 6/15/2015.
 */

public class Josepth {
    public static void main(String[] argv)
    {
        long startTime  = System.nanoTime();
        List<Integer> r;
        System.out.println(r = josephus(41, 3, 1));                     // show entire sequence
        long endTime = System.nanoTime();
        System.out.println("run time in nanoSeconds : " +(endTime-startTime));
        System.out.printf("Person %d is last\n", r.get(r.size() - 1));  // who's last?
    }

    // remove N elements in equal steps starting at specific point
    static List<Integer> josephus(int N, int step, int start)
    {
        if (N < 1 || step < 1 || start < 1) return null;

        List<Integer> p = new LinkedList<Integer>();
        for (int i = 0; i < N; i++)
            p.add(i+1);

        List<Integer> r = new LinkedList<Integer>();
        int i = (start - 2) % N;
        for (int j = N; j > 0; j--) {
            i = (i + step) % N--;
            r.add(p.remove(i--));
        }

        return r;
    }
}
