
/**
 * Name: Chelsea Smith
 * Class: CSCI 3200
 * Instructor: Justin Turner
 * Date: 06/02/2015
 * 1.15 � Define a Rectangle class that provides getLength() and getWidth() methods. Using the findMax(),
 * write a main() that creates an array of Rectangle objects and finds the largest Rectangle, first based on area,
 * then based on perimeter
 * */
//look at largest string example
import java.util.Comparator;
import java.util.Random;
public class rectangle {

    public static class Rectangle {
        private double width;
        private double height;
        private Random rP,rA;
        public void setHeight(double h) {
            height = h;
        }

        public void setWidth(double w) {
            width = w;
        }
        public double getArea() {
            return width * height;
        }

        public double getPerimeter() {
            return 2  * (width + height);
        }
        public String toString() {
            String S;
            S = "Width : " + width;
            S = S + " Height : " + height;
            return S;
        }
    }
    public static void main(String [ ] args){
        String[] colors = {"Pink","Blue","Yellow","Purple","Green", "Red", "White", "Orange", "Black", "Indigo"};
        Double[] ar = new Double[10];
        Double[] per = new Double[10];
        int k;
        for(int i = 0; i < 10; i++)
        {
            Rectangle r = new Rectangle();
            r.setWidth((Math.random()*40)+10);
            r.setHeight((Math.random()*40)+10);
            ar[i] = r.getArea();
            per[i] = r.getPerimeter();
            k = (int)(Math.random()*4)+1;
            System.out.println("Rectangle #" + (i+1) + " Color : " + colors[k]);
            System.out.println(r.toString() + " Area : " + r.getArea() + " Perimeter :  " + r.getPerimeter());

        }

        CaseInsensitiveCompare cmp = new CaseInsensitiveCompare();
        System.out.println("Max Area : " + findMax(ar,cmp));
        System.out.println("Max Perimeter : " + findMax(per,cmp));



    }
    private static <AnyType> AnyType findMax( AnyType[] arr, Comparator<? super AnyType> cmp )
    {
        int maxIndex = 0;
        for( int i = 1; i < arr.length; i++ )
            if( cmp.compare( arr[ i ], arr[ maxIndex ] ) > 0 )
                maxIndex = i;
        return arr[ maxIndex ];
    }
    public static class CaseInsensitiveCompare implements Comparator<Double>
    {
        public int compare( Double lhs, Double rhs )
        {
            return lhs > rhs ? 1 : 0;
        }
    }


}
