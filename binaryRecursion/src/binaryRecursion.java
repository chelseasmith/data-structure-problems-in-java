/**
 * Name: Chelsea Smith
 * Class: CSCI 3200
 * Instructor: Justin Turner
 * Date: 06/02/2015
 * 1.5 � Write a recursive method that returns the number of 1�s in a binary representation of N. Use the fact that this
 * is qual to the number of 1�s in the representation of N/2, plus 1, if N is odd
 * First binary number is 0
 * Second is 1 which equals 1
 * Third is 2 which equals 01
 * Fourth is 3 which equals 11
 * Fifth is 4 which equals...
 * */
import java.util.Scanner;
public class binaryRecursion {
    public static int numOfOnes = 0;
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int number = s.nextInt();
        getNumOfOnes(number);
        System.out.println(numOfOnes);
    }
    public static void getNumOfOnes (int number){
        if (number <= 1){
            if (number == 1){
                numOfOnes++;
            }
            return;
        }
        if (number % 2 !=0){
            numOfOnes++;
        }
         getNumOfOnes(number >> 1);
    }
}


