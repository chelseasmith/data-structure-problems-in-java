
public class inheritance {

    public static void main(String[] args)
    {
        parent[] p1 = new parent[5];
        p1[0] = new parent();
        p1[1] = new child1();
        p1[2] = new child2();

        parent[] p2 = new child1[5];
        p2[0] = new parent();
        p2[1] = new child1();
        p2[2] = new child2();
    }

    private static class parent{}
    private static class child1 extends parent{}
    private static class child2 extends parent{}

}
