import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class readFromFile {

    public static void main(String[] args) throws FileNotFoundException {
        long startTime = System.nanoTime();
		/*
		 * NOTE: In eclipse, the file should be in the root of the Java PROJECT
		 * For other tools, usually the input file goes in the same directory as the .java file
		 */
        Scanner s = new Scanner(new File("random1000000.txt"));
        //feel free to store the values however you want, here are a couple examples
		/*
		 * Fill an ArrayList
		 */
        /** /
         ArrayList<Integer> list = new ArrayList<Integer>();
         while(s.hasNextInt())
         list.add(s.nextInt());
         /**/
		/*
		 * Fill an array
		 */
        /** /
         int[] list = new int[1000000];
         for(int i = 0; i < 1000000; i++)
         list[i] = s.nextInt();
         /**/
        //feel free to add additional time checks if you are curious about how long certain processes take
        long endReadTime = System.nanoTime();
        //System.out.println(list.size());//size for ArrayList
        //System.out.println(list.length);//size for array
        /** /
         for(int i : list)
         System.out.println(i);
         /**/
        long endTime = System.nanoTime();
        //Convert nanoseconds to seconds, 1 second = 10^9 nanoseconds
        System.out.println("Read time in seconds: " + ((endReadTime-startTime)/Math.pow(10.0, 9)));
        System.out.println("Total runtime in seconds: " + ((endTime-startTime)/Math.pow(10.0, 9)));
    }

}
