
public class recursion {

    public static void main(String[] args) {
        recursive(5);

    }
    //f(x) = 2*f(x-1)+x^2
    public static int recursive(int x)
    {
        System.out.println("Start Method with: "+x);
        if(x == 0)
            return 0;
        else
        {
            System.out.println("Re-Run recursive method with " + (x-1));
            int temp = 2*recursive(x-1)+x*x;
            System.out.println("Answer from run of " + x +" : " + temp);
            return temp;
        }
    }

}
