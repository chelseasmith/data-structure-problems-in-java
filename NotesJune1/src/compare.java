import java.util.*;

public class compare
{
    public static void main( String [ ] args )
    {
        String [ ] arr = { "ZEBRA", "alligator", "crocodile" };
        System.out.println( findMax( arr, new CaseInsensitiveCompare( ) ) );
    }
    private static <AnyType> AnyType findMax( AnyType[] arr, Comparator<? super AnyType> cmp )
    {
        int maxIndex = 0;
        for( int i = 1; i < arr.length; i++ )
            if( cmp.compare( arr[ i ], arr[ maxIndex ] ) > 0 )
                maxIndex = i;
        return arr[ maxIndex ];
    }
    public static class CaseInsensitiveCompare implements Comparator<String>
    {
        private String lhs;
        private String rhs;

        public int compare( String lhs, String rhs )
        {
            return lhs.compareToIgnoreCase( rhs );
        }
    }
}