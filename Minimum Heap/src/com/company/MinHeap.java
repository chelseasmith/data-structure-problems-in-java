package com.company;
/**
 * Created by cjsmith1102 on 7/20/2015.
 * Assignment 10
 * The book provides heapsort code using a (max)heap instead of a (min)heap like we used in chapter 6.
 * Write your own heapsort using a (min)heap since we aren't worried about stoarge space.
 * (page 278-282 explains the (max)heap sort solution)
 */
public class MinHeap {
 private int[] Heap;
        private int size;
        private int maxsize;

        private static final int FRONT = 1;

        public MinHeap(int maxsize)
        {
            this.maxsize = maxsize;
            this.size = 0;
            Heap = new int[this.maxsize + 1];
            Heap[0] = Integer.MIN_VALUE;
        }

        private int parent(int pos)
        {
            return pos / 2;
        }

        private int leftChild(int pos)
        {
            return (2 * pos);
        }

        private int rightChild(int pos)
        {
            return (2 * pos) + 1;
        }

        private boolean isLeaf(int pos)
        {
            if (pos >=  (size / 2)  &&  pos <= size)
            {
                return true;
            }
            return false;
        }

        private void swap(int fpos, int spos)
        {
            int tmp;
            tmp = Heap[fpos];
            Heap[fpos] = Heap[spos];
            Heap[spos] = tmp;
        }

        private void minHeapify(int pos)
        {
            if (!isLeaf(pos))
            {
                if ( Heap[pos] > Heap[leftChild(pos)]  || Heap[pos] > Heap[rightChild(pos)])
                {
                    if (Heap[leftChild(pos)] < Heap[rightChild(pos)])
                    {
                        swap(pos, leftChild(pos));
                        minHeapify(leftChild(pos));
                    }else
                    {
                        swap(pos, rightChild(pos));
                        minHeapify(rightChild(pos));
                    }
                }
            }
        }

        public void insert(int element)
        {
            Heap[++size] = element;
            int current = size;

            while (Heap[current] < Heap[parent(current)])
            {
                swap(current,parent(current));
                current = parent(current);
            }
        }

        public void print()
        {
            for (int i = 1; i <= size / 2; i++ )
            {
                System.out.print(" PARENT : " + Heap[i] + " LEFT CHILD : " + Heap[2*i]
                        + " RIGHT CHILD :" + Heap[2 * i  + 1]);
                System.out.println();
            }
        }

        public void minHeap()
        {
            for (int pos = (size / 2); pos >= 1 ; pos--)
            {
                minHeapify(pos);
            }
        }

        public int remove()
        {
            int popped = Heap[FRONT];
            Heap[FRONT] = Heap[size--];
            minHeapify(FRONT);
            return popped;
        }

        public static void main(String...arg)
        {
            System.out.println("The Min Heap is ");
            MinHeap minHeap = new MinHeap(15);
            minHeap.insert(1000);
            minHeap.insert(10033434);
            minHeap.insert(23343343);
            minHeap.insert(234324);
            minHeap.insert(2000);
            minHeap.insert(348234343);
            minHeap.insert(34343434);
            minHeap.insert(83423834);
            minHeap.insert(342343242);
            minHeap.minHeap();

            minHeap.print();
            System.out.println("The Min val is " + minHeap.remove());
        }
}
