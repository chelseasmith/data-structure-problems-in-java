import java.util.Map;
import java.util.TreeMap;


public class Graph {
    TreeMap<String, Vertex> graph;
    public Graph()
    {
        graph = new TreeMap<String, Vertex>();
    }
    public void addEdge(String v1, String v2, int w)
    {
        //add vertices to graph
        addVertex(v1);
        addVertex(v2);

        //connect vertices
        if(graph.get(v1).addEdge(v2, w))
            graph.get(v2).indegree++;
    }
    //alias of addEdge(v1, v2, w)
    public void addEdge(String v1, String v2)
    {
        addEdge(v1, v2, 0);
    }
    //helper method to ensure vertex is in graph
    private void addVertex(String v)
    {
        if(!graph.containsKey(v))
        {
            graph.put(v, new Vertex(v));
        }
    }
    //helper method to remove a vertex with no vertices pointing to it
    private void removeVertex(String v)
    {
        //ensure vertex exists in graph and that it has no other vertices pointing to it
        if(graph.containsKey(v))
        {
            //if indegree is greater than zero, remove connections to this vertex
            if(graph.get(v).indegree > 0)
            {
                //look through all vertices in the graph
                for(Map.Entry<String, Vertex> vertex : graph.entrySet())
                {
                    String key = vertex.getKey();
                    //if this vertex connects to the one being removed, remove it from the adjacent list
                    //and decrease the indegree of the vertex being removed
                    if(graph.get(key).adjacent.containsKey(v))
                    {
                        graph.get(key).adjacent.remove(v);
                        graph.get(v).indegree--;
                    }
                    //end loop once indegree reaches zero
                    if(graph.get(v).indegree == 0)
                        break;
                }
            }
            //decrement indegree for all vertices this vertex points to
            for(Map.Entry<String, Integer> adjacent: graph.get(v).adjacent.entrySet())
            {
                graph.get(adjacent.getKey()).indegree--;
            }
            //remove vertex from graph
            graph.remove(v);
        }
    }
    public String toString()
    {
        if(graph.size() > 0)
        {
            String temp = "";
            for(Map.Entry<String, Vertex> vertex : graph.entrySet())
            {
                temp += vertex.getKey() + " : " + vertex.getValue() + "\n";
            }
            return temp;
        }
        return "No Vertices";
    }
}

