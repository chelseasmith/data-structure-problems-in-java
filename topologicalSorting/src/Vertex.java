/**
 * Created by cjsmith1102 on 7/24/2015.
 */
import java.util.ArrayList;
import java.util.TreeMap;


public class Vertex {
    public String vertex;//current vertex
    public int indegree;//number of vertices that lead to this vertex
    public TreeMap<String, Integer> adjacent;//list of vertices that can be reached from this vertex
    public Vertex(String v)
    {
        vertex = v;
        indegree = 0;
        adjacent = new TreeMap<String, Integer>();
    }
    public boolean addEdge(String v, int w)
    {
        //ensure the edge doesn't already exist based on weight and name
        if(!adjacent.containsKey(v))
        {
            adjacent.put(v, w);
            return true;
        }
        return false;
    }
    //alias of addEdge(v,w)
    public void addEdge(String v)
    {
        addEdge(v, 0);
    }
    public String toString()
    {
        String temp = vertex + " : " + indegree;
        if(adjacent.size() > 0)
        {
            temp += " : " + adjacent;
        }
        return temp;
    }
}

